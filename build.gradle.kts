plugins {
    kotlin("jvm") apply false
    java
    id("net.nemerosa.versioning")
}

allprojects {
    apply(plugin = "net.nemerosa.versioning")
//    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "org.gradle.maven-publish")

    group = "fe.koin-helper"
    version = versioning.info.tag ?: versioning.info.full
}
