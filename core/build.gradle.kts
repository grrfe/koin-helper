plugins {
    kotlin("jvm")
}

repositories {
    mavenCentral()
    maven(url = "https://jitpack.io")
}

dependencies {
    implementation(project(":api"))
    api(platform("com.github.1fexd:super"))
    api("com.gitlab.grrfe.kotlin-ext:lib")
    api("io.insert-koin:koin-core")
}

kotlin {
    jvmToolchain(17)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = project.group.toString()
            version = project.version.toString()

            from(components["java"])
        }
    }
}
