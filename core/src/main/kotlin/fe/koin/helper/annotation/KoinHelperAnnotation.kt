package fe.koin.helper.annotation

@RequiresOptIn(
    message = "Intended to be called from koin-helper, shouldn't be used by the user",
    level = RequiresOptIn.Level.ERROR
)
@Retention(AnnotationRetention.BINARY)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION, AnnotationTarget.FIELD, AnnotationTarget.PROPERTY)
annotation class KoinHelperInternalApi
