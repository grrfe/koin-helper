package fe.koin.helper.init

import fe.koin.helper.resolve.ResolveProxy
import fe.koin.helper.ext.*
import org.koin.core.Koin
import org.koin.core.error.NoBeanDefFoundException

typealias InitializerScope = KoinInitResolveProxy.() -> Unit

class KoinInitResolveProxy : ResolveProxy<KoinInit, Koin, Any>() {
    @Throws(NoBeanDefFoundException::class)
    inline fun <I : KoinInit1<T>, reified T : Any> init(
        init: I,
        options: KoinGetOptions? = null
    ) = add(init) { koin -> init.init(koin.get<T>(options?.first, options?.second)) }

    @Throws(NoBeanDefFoundException::class)
    inline fun <I : KoinInit2<T, T2>, reified T : Any, reified T2 : Any> init(
        init: I,
        options: KoinGetOptionsPair? = null,
    ) = add(init) { koin ->
        init.init(koin.get<T>(options?.first), koin.get<T2>(options?.second))
    }

    @Throws(NoBeanDefFoundException::class)
    inline fun <I : KoinInit3<T, T2, T3>, reified T : Any, reified T2 : Any, reified T3 : Any> init(
        init: I,
        options: KoinGetOptionsTriple? = null,
    ) = add(init) { koin ->
        init.init(koin.get<T>(options?.first), koin.get<T2>(options?.second), koin.get<T3>(options?.third))
    }

    @Throws(NoBeanDefFoundException::class)
    inline fun <I : KoinInit4<T, T2, T3, T4>, reified T : Any, reified T2 : Any, reified T3 : Any, reified T4 : Any> init(
        init: I,
        options: KoinGetOptionsQuadruple? = null,
    ) = add(init) { koin ->
        init.init(
            koin.get<T>(options?.first),
            koin.get<T2>(options?.second),
            koin.get<T3>(options?.third),
            koin.get<T4>(options?.fourth)
        )
    }

    @Throws(NoBeanDefFoundException::class)
    inline fun <I : KoinInit5<T, T2, T3, T4, T5>, reified T : Any, reified T2 : Any, reified T3 : Any, reified T4 : Any, reified T5 : Any> init(
        init: I,
        options: KoinGetOptionsQuintuple? = null,
    ) = add(init) { koin ->
        init.init(
            koin.get<T>(options?.first),
            koin.get<T2>(options?.second),
            koin.get<T3>(options?.third),
            koin.get<T4>(options?.fourth),
            koin.get<T5>(options?.fifth)
        )
    }
}
