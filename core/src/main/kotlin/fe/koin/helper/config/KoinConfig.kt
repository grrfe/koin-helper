package fe.koin.helper.config

import org.koin.core.definition.KoinDefinition
import org.koin.core.module.Module
import org.koin.core.qualifier.Qualifier
import kotlin.reflect.KProperty0

typealias ConfigScope<T> = ConfigDsl.(T) -> Unit

class NoSuchConfigException(config: String) : Exception("Could not find config $config")

class ConfigDsl(val module: Module) {
    inline fun <reified T : NullableKoinConfig<T>> nullableConfig(
        property: KProperty0<T?>,
        qualifier: Qualifier? = null
    ): KoinDefinition<T>? {
        val value = property.get() ?: return null
        return module.single<T>(qualifier) { value }
    }

    inline fun <reified T : KoinConfig<T>> config(
        property: KProperty0<T?>,
        qualifier: Qualifier? = null
    ): KoinDefinition<T> {
        val value = property.get() ?: throw NoSuchConfigException(property.name)
        return module.single<T>(qualifier) { value }
    }
}

inline fun <reified T : KoinRootConfig<T>> configureRootConfig(
    rootConfig: T,
    scope: ConfigScope<T>
): Module {
    return Module(true).apply {
        single<T> { rootConfig }
        scope(ConfigDsl(this), rootConfig)
    }
}
