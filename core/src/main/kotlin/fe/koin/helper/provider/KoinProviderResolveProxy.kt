package fe.koin.helper.provider

import fe.koin.helper.ext.KoinGetOptions
import fe.koin.helper.ext.KoinGetOptionsPair
import fe.koin.helper.ext.get
import fe.koin.helper.resolve.ResolveProxy
import org.koin.core.Koin
import org.koin.core.error.NoBeanDefFoundException
import org.koin.core.module.Module

typealias ProviderScope = KoinProviderResolveProxy.() -> Unit

class KoinProviderResolveProxy : ResolveProxy<KoinProvider, Pair<Module, Koin>, Any>() {
    @Throws(NoBeanDefFoundException::class)
    inline fun <reified O : Any, P : KoinProvider01<O>> provider(
        provider: P
    ) = add(provider) { (module, _) -> module.single<O> { provider.provide() } }

    @Throws(NoBeanDefFoundException::class)
    inline fun <reified O, P : KoinProvider11<I, O>, reified I : Any> provider(
        provider: P,
        getOptions: KoinGetOptions? = null
    ) = add(provider) { (module, koin) ->
        module.single<O> { provider.provide(koin.get<I>(getOptions?.first, getOptions?.second)) }
    }

    @Throws(NoBeanDefFoundException::class)
    inline fun <O, O2, P : KoinProvider12<I, O, O2>, reified I : Any> provider(
        provider: P,
        getOptions: KoinGetOptions? = null
    ) = add(provider) { (module, koin) ->
        module.single<Pair<O, O2>> { provider.provide(koin.get<I>(getOptions?.first, getOptions?.second)) }
    }

    @Throws(NoBeanDefFoundException::class)
    inline fun <reified O, P : KoinProvider21<I, I2, O>, reified I : Any, reified I2 : Any> provider(
        provider: P,
        getOptions: KoinGetOptionsPair? = null,
    ) = add(provider) { (module, koin) ->
        module.single<O> {
            provider.provide(koin.get<I>(getOptions?.first), koin.get<I2>(getOptions?.second))
        }
    }

    @Throws(NoBeanDefFoundException::class)
    inline fun <O, O2, P : KoinProvider22<I, I2, O, O2>, reified I : Any, reified I2 : Any> provider(
        provider: P,
        getOptions: KoinGetOptionsPair? = null,
    ) = add(provider) { (module, koin) ->
        module.single<Pair<O, O2>> {
            provider.provide(
                koin.get<I>(getOptions?.first),
                koin.get<I2>(getOptions?.second)
            )
        }
    }
}
