package fe.koin.helper.provider

import fe.koin.helper.util.KoinFailable


interface KoinProvider : KoinFailable

fun interface KoinProvider01<O> : KoinProvider {
    fun provide(): O
}

fun interface KoinProvider11<I, O> : KoinProvider {
    fun provide(i: I): O
}

fun interface KoinProvider12<I, O, O2> : KoinProvider {
    fun provide(i: I): Pair<O, O2>
}

fun interface KoinProvider21<I, I2, O> : KoinProvider {
    fun provide(i: I, i2: I2): O
}

fun interface KoinProvider22<I, I2, O, O2> : KoinProvider {
    fun provide(i: I, i2: I2): Pair<O, O2>
}


//interface KoinProvider31<I, I2, I3, O> : KoinProvider {
//    fun provide(i: I, i2: I2, i3: I3): O
//}
//
//interface KoinProvider32<I, I2, I3, O, O2> : KoinProvider {
//    fun provide(i: I, i2: I2, i3: I3): Pair<O, O2>
//}
//
//interface KoinProvider33<I, I2, I3, O, O2, O3> : KoinProvider {
//    fun provide(i: I, i2: I2, i3: I3): Triple<O, O2, O3>
//}
