package fe.koin.helper.ext

import fe.koin.helper.util.Nullable
import org.koin.core.Koin
import org.koin.core.parameter.ParametersDefinition
import org.koin.core.qualifier.Qualifier

typealias KoinGetOptions = Pair<Qualifier?, ParametersDefinition?>

inline fun <reified T : Any> Koin.get(options: KoinGetOptions? = null): T = get(options?.first, options?.second)

inline fun <reified T : Any> Koin.getNullable(
    qualifier: Qualifier? = null,
    noinline parameters: ParametersDefinition? = null
): T? = get<Nullable<T>>(qualifier, parameters).value

inline fun <reified T : Any> Koin.getNullable(
    options: KoinGetOptions? = null
): T? = get<Nullable<T>>(options?.first, options?.second).value
