package fe.koin.helper.ext

import fe.koin.helper.util.Nullable
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.parameter.ParametersDefinition
import org.koin.core.qualifier.Qualifier
import org.koin.mp.KoinPlatformTools


inline fun <reified T : Any> KoinComponent.injectNullable(
    qualifier: Qualifier? = null,
    mode: LazyThreadSafetyMode = KoinPlatformTools.defaultLazyMode(),
    noinline parameters: ParametersDefinition? = null
): Lazy<T?> {
    return lazy(mode) {
        get<Nullable<T>>(qualifier, parameters).value
    }
}
