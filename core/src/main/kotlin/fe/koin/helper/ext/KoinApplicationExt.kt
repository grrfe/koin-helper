package fe.koin.helper.ext

import fe.kotlin.util.Quadruple
import fe.kotlin.util.Quintuple


typealias KoinGetOptionsPair = Pair<KoinGetOptions?, KoinGetOptions?>
typealias KoinGetOptionsTriple = Triple<KoinGetOptions?, KoinGetOptions?, KoinGetOptions?>
typealias KoinGetOptionsQuadruple = Quadruple<KoinGetOptions?, KoinGetOptions?, KoinGetOptions?, KoinGetOptions?>
typealias KoinGetOptionsQuintuple = Quintuple<KoinGetOptions?, KoinGetOptions?, KoinGetOptions?, KoinGetOptions?, KoinGetOptions?>
