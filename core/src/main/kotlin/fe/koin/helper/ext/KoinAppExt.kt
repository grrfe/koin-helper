package fe.koin.helper.ext

import fe.koin.helper.app.KoinApp
import fe.koin.helper.app.configuration.KoinAppConfiguration
import fe.koin.helper.config.ConfigScope
import fe.koin.helper.config.KoinRootConfig
import fe.koin.helper.config.configureRootConfig
import fe.koin.helper.init.InitializerScope
import fe.koin.helper.init.KoinInitResolveProxy
import fe.koin.helper.provider.KoinProviderResolveProxy
import fe.koin.helper.provider.ProviderScope
import org.koin.core.KoinApplication
import org.koin.core.context.KoinContext
import org.koin.core.module.Module
import org.koin.mp.KoinPlatformTools

inline fun <C : KoinAppConfiguration<T>, reified T : KoinRootConfig<T>> C.configure(
    inputRootConfig: T,
    startOnContext: KoinContext? = KoinPlatformTools.defaultContext(),
    noinline configScope: ConfigScope<T> = {},
    noinline providers: ProviderScope = {},
    noinline initializers: InitializerScope = {},
    modules: Array<Module> = emptyArray<Module>(),
): KoinApplication {
    val koinApplication = createKoinApplication()

    val (rootConfig, receivedConfigScope) = getConfig(koinApplication, inputRootConfig, configScope)
    val configModule = afterConfigConfiguration(
        koinApplication, configureRootConfig<T>(rootConfig, receivedConfigScope)
    )

    koinApplication.modules(configModule)
    koinApplication.modules(getModules(koinApplication, modules))

    val (providerModule, providerScope) = getProviders(koinApplication, Module(true), providers)

    KoinProviderResolveProxy()
        .apply(providerScope)
        .resolve(providerModule to koinApplication.koin)

    koinApplication.modules(afterProviderConfiguration(koinApplication, providerModule))

    KoinInitResolveProxy().apply(getInitializers(koinApplication, initializers)).resolve(koinApplication.koin)

    startOnContext?.startKoin(koinApplication)
    return koinApplication
}

inline fun <reified T : KoinRootConfig<T>> KoinApp<T>.start(inputRootConfig: T): KoinApplication {
    return configure(
        inputRootConfig,
        configScope = configScope,
        providers = providers,
        initializers = initializers,
        modules = modules
    )
}
