package fe.koin.helper.resolve

object ResolveCalledMoreThanOnceException : Exception("Resolve may not be called more than once!") {
    private fun readResolve(): Any = ResolveCalledMoreThanOnceException
}
