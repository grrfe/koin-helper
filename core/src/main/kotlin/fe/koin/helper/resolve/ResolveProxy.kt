package fe.koin.helper.resolve

class DuplicateToResolveRegistered(toResolve: Any) : Exception("Duplicate $toResolve registered!")

abstract class ResolveProxy<T : Any, I : Any, O> {
    private val registered = mutableMapOf<T, (I) -> O>()
    private var resolved: Boolean = false

    fun add(t: T, fn: (I) -> O) {
        if (registered[t] != null) throw DuplicateToResolveRegistered(t)
        registered[t] = fn
    }

    @Throws(ResolveCalledMoreThanOnceException::class)
    fun resolve(instance: I): I {
        if (resolved) throw ResolveCalledMoreThanOnceException

        registered.values.forEach { it.invoke(instance) }
        resolved = true
        return instance
    }
}
