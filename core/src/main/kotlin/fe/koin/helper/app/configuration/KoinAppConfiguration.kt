package fe.koin.helper.app.configuration

import fe.koin.helper.config.ConfigScope
import fe.koin.helper.config.KoinRootConfig
import fe.koin.helper.init.InitializerScope
import fe.koin.helper.provider.ProviderScope
import org.koin.core.KoinApplication
import org.koin.core.module.Module

interface KoinAppConfiguration<T : KoinRootConfig<T>> {
    fun createKoinApplication(): KoinApplication

    fun getConfig(
        koinApplication: KoinApplication,
        rootConfig: T,
        configScope: ConfigScope<T>
    ): Pair<T, ConfigScope<T>>

    fun afterConfigConfiguration(koinApplication: KoinApplication, module: Module): Module

    fun getModules(koinApplication: KoinApplication, modules: Array<Module>): List<Module>

    fun getProviders(
        koinApplication: KoinApplication,
        module: Module,
        providers: ProviderScope
    ): Pair<Module, ProviderScope>

    fun afterProviderConfiguration(
        koinApplication: KoinApplication,
        module: Module
    ): Module

    fun getInitializers(
        koinApplication: KoinApplication,
        initializerScope: InitializerScope
    ): InitializerScope
}
