package fe.koin.helper.app.configuration

import fe.koin.helper.config.ConfigScope
import fe.koin.helper.config.KoinRootConfig
import fe.koin.helper.init.InitializerScope
import fe.koin.helper.provider.ProviderScope
import org.koin.core.KoinApplication
import org.koin.core.module.Module

open class DefaultKoinAppConfiguration<T : KoinRootConfig<T>> : KoinAppConfiguration<T> {
    override fun getConfig(
        koinApplication: KoinApplication,
        rootConfig: T,
        configScope: ConfigScope<T>
    ): Pair<T, ConfigScope<T>> = rootConfig to configScope

    override fun createKoinApplication(): KoinApplication = KoinApplication.init()

    override fun afterConfigConfiguration(koinApplication: KoinApplication, module: Module): Module = module

    override fun getModules(koinApplication: KoinApplication, modules: Array<Module>): List<Module> = modules.toList()

    override fun getProviders(
        koinApplication: KoinApplication,
        module: Module,
        providers: ProviderScope
    ): Pair<Module, ProviderScope> = module to providers

    override fun afterProviderConfiguration(
        koinApplication: KoinApplication,
        module: Module
    ): Module = module

    override fun getInitializers(
        koinApplication: KoinApplication,
        initializerScope: InitializerScope
    ): InitializerScope = initializerScope
}
