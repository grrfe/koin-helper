package fe.koin.helper.app

import fe.koin.helper.app.configuration.DefaultKoinAppConfiguration
import fe.koin.helper.app.configuration.KoinAppConfiguration
import fe.koin.helper.config.ConfigScope
import fe.koin.helper.config.KoinRootConfig
import fe.koin.helper.init.InitializerScope
import fe.koin.helper.provider.ProviderScope
import org.koin.core.module.Module

abstract class KoinApp<T : KoinRootConfig<T>>(
    private val configuration: KoinAppConfiguration<T> = DefaultKoinAppConfiguration()
) : KoinAppConfiguration<T> by configuration {

    abstract val configScope: ConfigScope<T>
    abstract val modules: Array<Module>
    abstract val providers: ProviderScope
    abstract val initializers: InitializerScope
}
