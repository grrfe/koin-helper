package fe.koin.helper.util

import org.koin.core.definition.Definition
import org.koin.core.module.Module
import org.koin.core.qualifier.Qualifier

inline fun <reified T> singleModule(
    module: Module = Module(),
    qualifier: Qualifier? = null,
    createdAtStart: Boolean = false,
    noinline definition: Definition<T>
): Module = module.apply {
    single(qualifier, createdAtStart, definition)
}
