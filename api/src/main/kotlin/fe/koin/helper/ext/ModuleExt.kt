package fe.koin.helper.ext

import fe.koin.helper.config.KoinConfig
import fe.koin.helper.config.NullableKoinConfig
import fe.koin.helper.util.Nullable
import org.koin.core.definition.KoinDefinition
import org.koin.core.module.Module
import org.koin.core.parameter.ParametersHolder
import org.koin.core.qualifier.Qualifier
import org.koin.core.scope.Scope

inline fun <reified C : NullableKoinConfig<C>, reified T> Module.singleFromNullableConfig(
    qualifier: Qualifier? = null,
    createdAtStart: Boolean = false,
    crossinline definition: Scope.(ParametersHolder, C) -> T
): KoinDefinition<Nullable<T>> {
    return single<Nullable<T>>(qualifier, createdAtStart) {
        val config = getOrNull<C>() ?: return@single Nullable()
        Nullable(definition(it, config))
    }
}

inline fun <reified C : KoinConfig<C>, reified T> Module.singleConfig(
    qualifier: Qualifier? = null,
    createdAtStart: Boolean = false,
    crossinline definition: Scope.(ParametersHolder, C) -> T
): KoinDefinition<T> {
    return single<T>(qualifier, createdAtStart) {
        definition(it, get<C>())
    }
}
