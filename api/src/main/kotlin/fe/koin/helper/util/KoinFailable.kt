package fe.koin.helper.util

class KoinFailException(message: String, throwable: Throwable? = null) : Exception(message, throwable)

interface KoinFailable {
    fun fail(reason: String, throwable: Throwable? = null): Nothing = throw KoinFailException(reason, throwable)

    fun tryOrFail(reason: String, includeThrowable: Boolean = true, block: KoinFailable.() -> Unit) {
        runCatching(block).onFailure { fail(reason, if (includeThrowable) it else null) }
    }
}
