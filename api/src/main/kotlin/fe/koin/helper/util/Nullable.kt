package fe.koin.helper.util

class Nullable<T>(val value: T? = null)
