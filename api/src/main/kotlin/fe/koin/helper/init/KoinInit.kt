package fe.koin.helper.init

import fe.koin.helper.util.KoinFailable

interface KoinInit : KoinFailable

fun interface KoinInit0 : KoinInit {
    fun init()
}

fun interface KoinInit1<T> : KoinInit {
    fun init(t: T)
}

fun interface KoinInit2<T, T2> : KoinInit {
    fun init(t: T, t2: T2)
}

fun interface KoinInit3<T, T2, T3> : KoinInit {
    fun init(t: T, t2: T2, t3: T3)
}

fun interface KoinInit4<T, T2, T3, T4> : KoinInit {
    fun init(t: T, t2: T2, t3: T3, t4: T4)
}

fun interface KoinInit5<T, T2, T3, T4, T5> : KoinInit {
    fun init(t: T, t2: T2, t3: T3, t4: T4, t5: T5)
}
