package fe.koin.helper.config

interface KoinConfig<T>
interface KoinRootConfig<T> : KoinConfig<T>
interface NullableKoinConfig<T> : KoinConfig<T?>
