plugins {
    kotlin("jvm")
}

repositories {
    mavenCentral()
}

dependencies {
    api(platform("com.github.1fexd:super"))
    api("io.insert-koin:koin-core")
}

kotlin {
    jvmToolchain(17)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = project.group.toString()
            version = project.version.toString()

            from(components["java"])
        }
    }
}
