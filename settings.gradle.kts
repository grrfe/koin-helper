rootProject.name = "koin-helper"

pluginManagement {
    repositories {
        gradlePluginPortal()
    }

    plugins {
        kotlin("jvm") version "1.9.24"
        id("de.fayard.refreshVersions") version "0.60.5"
        id("org.gradle.maven-publish")
        id("net.nemerosa.versioning") version "3.1.0"
    }
}

plugins {
    id("de.fayard.refreshVersions")
}

include("core", "api")

if (System.getenv("DISABLE_TESTING_MODULE")?.toBooleanStrictOrNull() != false) {
//    include("testing")
}
